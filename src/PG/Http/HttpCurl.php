<?php

namespace PG\Http;

/**
 * Class HttpCurl
 *
 * @package PG\Http
 * @since v1.0.0
 */
class HttpCurl
{
    protected $curl;

    /**
     * HttpCurl constructor
     *
     * @param string $url
     * @param string $method ['GET', 'POST']
     * @param array $body
     * @param array $headers
     * @param int $timeOut
     */
    public function __construct($url, $method, $body, array $headers = [], $timeOut = 30)
    {
        $this->curl = curl_init();

        curl_setopt_array($this->curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => http_build_query($body),
            CURLOPT_HTTPHEADER => $this->compileRequestHeaders($headers)
        ));
    }

    /**
     * Convert array header to http headers
     *
     * @param array $headers
     * @return array
     */
    private function compileRequestHeaders(array $headers)
    {
        $return = [];

        foreach ($headers as $key => $value) {
            $return[] = $key . ': ' . $value;
        }

        return $return;
    }

    /**
     * Close curl
     */
    public function close()
    {
        curl_close($this->curl);
    }

    /**
     * Run a curl request
     *
     * @return mixed
     */
    public function exec()
    {
        return curl_exec($this->curl);
    }
}