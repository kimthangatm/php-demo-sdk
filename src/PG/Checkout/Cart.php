<?php

namespace PG\PGCheckout;

/**
 * Class Cart
 *
 * @package PG
 * @since v1.0.0
 */
class Cart
{
    /**
     * @var string
     */
    private $order_id;

    /**
     * @var Product[]
     */
    private $products = [];

    /**
     * Total cart price
     *
     * Include: fee_shipping, tax
     *
     * @var int
     */
    private $total_product_price = 0;

    /**
     * Fee shipping
     *
     * @var int
     */
    private $fee_shipping = 0;

    /**
     * Currency
     *
     * Now we accept only VN
     *
     * @var string
     */
    private $currency = 'VN';

    /**
     * @var integer
     */
    private $total_price;

    /**
     * Tax
     *
     * @var float
     */
    private $tax = 0.0;

    /**
     * Total type product in cart
     * --------------------------
     * Eg:
     *  Egg: 2
     *  Pen: 1
     *
     * So result: 2
     *
     * @var int
     */
    private $quantity = 0;

    /**
     * Total product item in cart
     * --------------------------
     * Eg:
     *  Egg: 2
     *  Pen: 1
     *
     * So result: 3
     *
     * @var int
     */
    private $quantity_item = 0;

    /**
     * Cart constructor
     *
     * @param string $order_id
     * @param array [Pro $products
     */
    public function __construct($order_id, $products = [])
    {
        $this->setOrderId($order_id);
        $this->setProducts($products);
        $this->processData();
    }

    /**
     * @return string
     */
    public function getOrderId()
    {
        return $this->order_id;
    }

    /**
     * @param string $order_id
     * @return $this
     */
    public function setOrderId($order_id)
    {
        $this->order_id = $order_id;
        return $this;
    }

    /**
     * @return array
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * @param array $products
     * @return $this
     */
    public function setProducts($products)
    {
        $this->products = $products;
        return $this;
    }

    /**
     * @return int
     */
    public function getTotalProductPrice()
    {
        return $this->total_product_price;
    }

    /**
     * @param int $total_product_price
     */
    public function setTotalProductPrice($total_product_price)
    {
        $this->total_product_price = $total_product_price;
    }

    /**
     * @return int
     */
    public function getFeeShipping()
    {
        return $this->fee_shipping;
    }

    /**
     * @param int $fee_shipping
     * @return $this
     */
    public function setFeeShipping($fee_shipping)
    {
        $this->fee_shipping = $fee_shipping;
        return $this;
    }

    /**
     * @return string
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param string $currency
     * @return $this
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;
        return $this;
    }

    /**
     * @return float
     */
    public function getTax()
    {
        return $this->tax;
    }

    /**
     * @param float $tax
     * @return $this
     */
    public function setTax($tax)
    {
        $this->tax = $tax;
        return $this;
    }

    /**
     * @return int
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @return int
     */
    public function getQuantityItem()
    {
        return $this->quantity_item;
    }

    private function processData()
    {
        $this->quantity = count($this->products);
        foreach ($this->products as $product) {
            $this->total_product_price += $product->getPrice() * $product->getQuantity();
            $this->quantity_item += $product->getQuantity();
        }

        $this->total_price = $this->total_product_price - ($this->tax + $this->fee_shipping);
    }
}