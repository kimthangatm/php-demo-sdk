<?php

namespace PG;

/**
 * Class Define
 *
 * @package PG
 * @since v1.0.0
 */
class Define
{
    const PG_VERSION = 'v1';

    const ENDPOINT_URL = 'https://payment.ongate.vn';
//    const SANDBOX_ENDPOINT_URL = 'https://sandbox.payment.net2e.vn';
    const SANDBOX_ENDPOINT_URL = 'http://127.0.0.1:8000';

    const OAUTH_AUTHORIZE_URL = 'oauth/authorize';
    const OAUTH_AUTHORIZE_SANDBOX_URL = 'oauth/authorize';

    const OAUTH_TOKEN_URL = 'oauth/token';
    const OAUTH_TOKEN_SANDBOX_URL = 'oauth/token';

    const OAUTH_API_USER_URL = 'api/user';
    const OAUTH_API_USER_SANDBOX_URL = 'api/user';

    const CHECKOUT_CART_URL = 'api/fast_checkout';
    const CHECKOUT_CART_SANDBOX_URL = 'api/fast_checkout';

    const VERIFY_CART_URL = 'api/fast_checkout';
    const VERIFY_CART_SANDBOX_URL = 'api/verify_checkout';

    public static function getOAuthAuthorizeURL($sandbox = false)
    {
        return $sandbox ? self::url(self::OAUTH_AUTHORIZE_URL, null)
            : self::sandboxUrl(self::OAUTH_AUTHORIZE_SANDBOX_URL, null);
    }

    public static function getOAuthTokenURL($sandbox = false)
    {
        return $sandbox ? self::url(self::OAUTH_TOKEN_URL, null)
            : self::sandboxUrl(self::OAUTH_TOKEN_SANDBOX_URL, null);
    }

    public static function getOAuthAPIUserURL($sandbox = false)
    {
        return $sandbox ? self::url(self::OAUTH_API_USER_URL, null)
            : self::sandboxUrl(self::OAUTH_API_USER_SANDBOX_URL, null);
    }

    public static function getCheckoutCartURL($sandbox = false)
    {
        return $sandbox ? self::url(self::CHECKOUT_CART_URL) : self::sandboxUrl(self::CHECKOUT_CART_SANDBOX_URL);
    }

    public static function getVerifyCartURL($sandbox = false)
    {
        return $sandbox ? self::url(self::VERIFY_CART_URL) : self::sandboxUrl(self::VERIFY_CART_SANDBOX_URL);
    }

    private static function url($part, $version = null)
    {
        if (!$version) {
            $version = '/';
        } else {
            $version = '/' . self::PG_VERSION . '/';
        }
        return self::ENDPOINT_URL . $version . $part;
    }

    private static function sandboxUrl($part, $version = null)
    {
        if (!$version) {
            $version = '/';
        } else {
            $version = '/' . self::PG_VERSION . '/';
        }
        return self::SANDBOX_ENDPOINT_URL . $version . $part;
    }
}