<?php

namespace PG\PGCheckout;

/**
 * Class Product
 *
 * @package PG
 * @since v1.0.0
 */
class Product
{
    /**
     * Product name
     *
     * @var string
     */
    private $name;

    /**
     * Product price
     *
     * @var integer
     */
    private $price;

    /**
     * Product quantity
     *
     * @var integer
     */
    private $quantity;

    /**
     * Product image url
     *
     * @var string
     */
    private $imageURL;

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return int
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param int $price
     *
     * @return $this
     */
    public function setPrice($price)
    {
        $this->price = $price;
        return $this;
    }

    /**
     * @return int
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param int $quantity
     *
     * @return $this
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
        return $this;
    }

    /**
     * @return string
     */
    public function getImageURL()
    {
        return $this->imageURL;
    }

    /**
     * @param string $imageURL
     *
     * @return $this
     */
    public function setImageURL($imageURL)
    {
        $this->imageURL = $imageURL;
        return $this;
    }
}