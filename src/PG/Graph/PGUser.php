<?php

namespace PG\Graph;

/**
 * Class PGUser
 *
 * @package PG\Graph
 * @since v1.0.0
 */
class PGUser
{
    public $id;
    public $name;
    public $nickname;
    public $email;
    public $avatar;

    /**
     * PGUser constructor
     *
     * @param string|array $response
     */
    public function __construct($response)
    {
        if (is_string($response)) {
            $response = json_decode($response, true);
        }
        $this->id = $response['id'];
        $this->nickname = isset($response['username']) ? $response['username'] : $this->makeUsernameFromEmail($response['email']);
        $this->name = isset($response['name']) ? $response['name'] : $this->makeUsernameFromEmail($response['email']);
        $this->email = $response['email'];
        $this->avatar = isset($response['avatar']) ? $response['avatar'] : null;
    }

    private function makeUsernameFromEmail($email)
    {
        $parts = explode('@', $email);
        return $parts[0];
    }
}