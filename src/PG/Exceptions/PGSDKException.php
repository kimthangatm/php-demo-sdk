<?php

namespace PG\Exceptions;

/**
 * Class PGSDKException
 *
 * @package PG
 * @since v1.0.0
 */
class PGSDKException extends \Exception
{

}