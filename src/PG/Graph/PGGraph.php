<?php

namespace PG\Graph;

use GuzzleHttp\Client;
use PG\Define;
use PG\Exceptions\PGOAuthenticationException;
use PG\Exceptions\PGSDKException;
use PG\Http\HttpCurl;
use PG\OAuth\AccessToken;
use PG\PG;

/**
 * Class GraphUser
 *
 * @package PG
 * @since v1.0.0
 */
class PGGraph
{
    protected $access_token;
    protected $config;
    protected $request_timeout;

    /**
     * PGGraph constructor
     *
     * @param array $config
     * @param AccessToken|string $access_token
     * @param int $request_timeout
     */
    public function __construct($config, $access_token, $request_timeout = 10)
    {
        $this->config = $config;
        if (is_string($access_token)) {
            $access_token = AccessToken::newInstance($access_token);
        }
        $this->access_token = $access_token;
    }

    public function getMe()
    {
        return $this->getUserInfo();
    }

    /**
     * Get user info
     *
     * @return PGUser
     */
    public function getUserInfo()
    {
        $headers = ['Authorization' => $this->access_token->getAccessToken()];
        if ($this->config['request_type'] === PG::REQUEST_TYPE_CURL) {
            $request = new HttpCurl(Define::getOAuthAPIUserURL(), 'GET', [], $headers, $this->request_timeout);
            $response = $request->exec();
            $request->close();
            return $this->getUserInfoFromResponse($response);
        } else {
            $http = new Client();
            $response = $http->post(Define::getOAuthTokenURL(), [
                'headers' => $headers,
                'form_params' => [],
            ]);
            return $this->getUserInfoFromResponse($response);
        }
    }

    /**
     * Get user info from response pg api user info
     *
     * @param $response
     * @return PGUser
     * @throws PGSDKException
     */
    private function getUserInfoFromResponse($response)
    {
        try {
            $response_array = json_decode((string)$response, true);
            if ($response_array['id']) {
                return new PGUser($response);
            } else {
                throw new PGOAuthenticationException();
            }
        } catch (PGSDKException $e) {
            throw $e;
        }
    }
}