<?php

namespace PG\Exceptions;

/**
 * Class PGOAuthenticationException
 *
 * @package PG
 * @since v1.0.0
 */
class PGOAuthenticationException extends PGSDKException
{

}