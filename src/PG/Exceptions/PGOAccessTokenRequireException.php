<?php

namespace PG\Exceptions;

/**
 * Class PGOAccessTokenRequireException
 *
 * @package PG
 * @since v1.0.0
 */
class PGOAccessTokenRequireException extends PGSDKException
{

}