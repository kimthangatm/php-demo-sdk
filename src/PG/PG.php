<?php

namespace PG;

use PG\Exceptions\PGOAccessTokenRequireException;
use PG\Graph\PGGraph;
use PG\OAuth\AccessToken;
use PG\OAuth\OAuth;
use PG\PGCheckout\Cart;
use PG\PGCheckout\Checkout;

/**
 * Class PG
 *
 * @package PG
 * @since v1.0.0
 */
class PG
{
    const REQUEST_TYPE_CURL = 'CURL';
    const REQUEST_TYPE_HTTP = 'HTTP';

    /**
     * @var PG
     */
    public static $instance;

    /**
     * Production or sandbox environment
     *
     * @var bool
     */
    private $sandbox;

    /**
     * @var array
     */
    protected $config;

    /**
     * @var AccessToken;
     */
    protected $accessToken;

    /**
     * PG constructor
     *
     * @param array $config
     */
    public function __construct($config)
    {
        $this->config = array_merge([
            'merchant_id' => null,
            'merchant_secret' => null,
            'oauth_secret' => null,
            'oauth_callback' => null,
            'language' => 'vi',
            'request_type' => PG::REQUEST_TYPE_CURL
        ], $config);

        $this->config['oauth_client_id'] = $this->config['merchant_id'];
    }

    public static function getInstance($config = null)
    {
        if (self::$instance === null) {
            return new PG($config);
        } else {
            return self::$instance;
        }
    }

    /**
     * Create cart
     *
     * @param string $order_id
     *
     * @return Cart
     */
    public function createCart($order_id)
    {
        return new Cart($order_id);
    }

    /**
     * @param Cart $cart
     *
     * @return Checkout
     */
    public function createCheckout($cart)
    {
        return new Checkout($cart, $this->config, $this->isSandbox());
    }

    /**
     * Create OAuth
     *
     * @param int $request_timeout
     * @return OAuth
     */
    public function createOAuth($request_timeout = 10)
    {
        return new OAuth($this->config, $request_timeout);
    }


    /**
     * Create graph
     *
     * @param AccessToken $access_token
     * @return PGGraph
     * @throws PGOAccessTokenRequireException
     */
    public function createGraph($access_token = null)
    {
        $access_token = $access_token ? $access_token : PG::getInstance()->getAccessToken();
        if (!$access_token) {
            return new PGGraph($access_token);
        } else {
            throw new PGOAccessTokenRequireException();
        }
    }

    /**
     * @return bool
     */
    public function isSandbox()
    {
        return $this->sandbox;
    }

    /**
     * @param bool $sandbox
     */
    public function setSandbox($sandbox)
    {
        $this->sandbox = $sandbox;
    }

    /**
     * @param AccessToken $accessToken
     */
    public function setAccessToken($accessToken)
    {
        $this->accessToken = $accessToken;
    }

    /**
     * @return AccessToken
     */
    public function getAccessToken()
    {
        return $this->accessToken;
    }


}