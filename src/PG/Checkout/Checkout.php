<?php

namespace PG\PGCheckout;

use GuzzleHttp\Client;
use PG\Define;
use PG\Http\HttpCurl;
use PG\PG;

/**
 * Class Checkout
 *
 * @package PG
 * @since v1.0.0
 */
class Checkout
{
    private $config;

    /**
     * @var Cart
     */
    private $cart;
    private $is_sandbox;
    private $affiliate_code;
    private $request_headers = [];
    private $request_timeout = 10;

    /**
     * @var array
     */
    protected $params_checkout;

    public function __construct($cart, $config, $is_sandbox)
    {
        $this->cart = $cart;
        $this->is_sandbox = $is_sandbox;
        $this->config = $config;
    }

    public function verifyCart()
    {
        if ($this->config['request_type'] === PG::REQUEST_TYPE_CURL) {
            $request = new HttpCurl(Define::getVerifyCartURL(), 'POST', $this->$this->getCheckoutParams(true),
                $this->request_headers, $this->request_timeout);
            $response = $request->exec();
            $request->close();
            return json_decode($response);
        } else {
            $http = new Client;
            $response = $http->post(Define::getVerifyCartURL(), [
                'form_params' => $this->$this->getCheckoutParams(true)
            ]);

            return json_decode((string)$response->getBody(), true);
        }
    }

    public function checkoutCart()
    {
        if ($this->config['request_type'] === PG::REQUEST_TYPE_CURL) {
            $request = new HttpCurl(Define::getCheckoutCartURL(), 'POST', $this->getCheckoutParams(false),
                $this->request_headers, $this->request_timeout);
            $response = $request->exec();
            $request->close();

            return json_decode($response);
        } else {
            $http = new Client;
            $response = $http->post(Define::getCheckoutCartURL(), [
                'form_params' => $this->getCheckoutParams(false)
            ]);

            return json_decode((string)$response->getBody(), true);
        }
    }

    /**
     * Process data for constructor
     *
     * @param bool $is_verify
     * @return array
     */
    private function getCheckoutParams($is_verify = false)
    {
        $params = [
            'lang' => $this->config['lang'],
            'merchant_id' => $this->config['merchant_id'],
            'return_url' => null,
            'cancel_url' => null,
            'affiliate_code' => $this->affiliate_code,
            'order_id' => $this->cart->getOrderId(),
            'quantity' => $this->cart->getQuantityItem(),
            'total_price' => $this->cart->getTotalProductPrice(),
            'tax' => $this->cart->getTax(),
            'fee_shipping' => $this->cart->getFeeShipping(),
            'order_description' => null,
            'products' => json_encode($this->cart->getProducts())
        ];

        if ($is_verify) {
            $params['is_verify'] = 1;
        }

        foreach ($params as $key => $param) {
            $params[$key] = strval($params[$key]);
        }

        $signature = md5($this->config['merchant_secret'] . '|' . implode('|', $params));

        $params['signature'] = $signature;

        return $params;
    }

    /**
     * @return array
     */
    public function getRequestHeaders()
    {
        return $this->request_headers;
    }

    /**
     * @param array $request_headers
     */
    public function setRequestHeaders($request_headers)
    {
        $this->request_headers = $request_headers;
    }

    /**
     * @return int
     */
    public function getRequestTimeout()
    {
        return $this->request_timeout;
    }

    /**
     * @param int $request_timeout
     */
    public function setRequestTimeout($request_timeout)
    {
        $this->request_timeout = $request_timeout;
    }
}