<?php

namespace PG\Exceptions;

/**
 * Class PGOAuthorizationException
 *
 * @package PG
 * @since v1.0.0
 */
class PGOAuthorizationException extends PGSDKException
{

}