<?php

namespace PG\OAuth;

/**
 * Class AccessToken
 *
 * @package PG\OAuth
 * @since v1.0.0
 */
class AccessToken
{
    /**
     * @var string
     */
    protected $token_type;

    /**
     * @var string
     */
    protected $expires_in;

    /**
     * @var string
     */
    protected $access_token;

    /**
     * @var string
     */
    protected $refresh_token;

    /**
     * AccessToken constructor
     *
     * @param array|string $response
     */
    public function __construct($response)
    {
        if (is_array($response)) {
            $this->token_type = $response['token_type'];
            $this->expires_in = $response['expires_in'];
            $this->access_token = $response['access_token'];
            $this->refresh_token = $response['refresh_token'];
        } else {
            $response = json_decode($response);
            $this->token_type = $response['token_type'];
            $this->expires_in = $response['expires_in'];
            $this->access_token = $response['access_token'];
            $this->refresh_token = $response['refresh_token'];
        }
    }

    public static function newInstance($access_token, $expires_in = 0, $refresh_token = null, $token_type = null)
    {
        return new AccessToken([
            'access_token' => $access_token,
            'expires_in' => $expires_in,
            'refresh_token' => $refresh_token,
            'token_type' => $token_type
        ]);
    }

    /**
     * @return mixed
     */
    public function getTokenType()
    {
        return $this->token_type;
    }

    /**
     * @return mixed
     */
    public function getExpiresIn()
    {
        return $this->expires_in;
    }

    /**
     * @return mixed
     */
    public function getAccessToken()
    {
        return $this->access_token;
    }

    /**
     * @return mixed
     */
    public function getRefreshToken()
    {
        return $this->refresh_token;
    }
}