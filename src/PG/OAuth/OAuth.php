<?php

namespace PG\OAuth;

use GuzzleHttp\Client;
use PG\Exceptions\PGOAuthenticationException;
use PG\Exceptions\PGOAuthorizationException;
use PG\Define;
use PG\Http\HttpCurl;
use PG\PG;

/**
 * Class OAuth
 *
 * @package PG
 * @since v1.0.0
 */
class OAuth
{

    /**
     * @var array
     */
    protected $config;

    /**
     * @var int
     */
    protected $request_timeout;

    /**
     * PGOAuth constructor
     *
     * @param array $config
     * @param integer $request_timeout
     */
    public function __construct($config, $request_timeout)
    {
        $this->config = $config;
        $this->request_timeout = $request_timeout;
    }

    /**
     * Generates an authorization URL
     *
     * @param array $scope Permissions to request
     * @param string $state CSRF value. You can make a state with a random text function from server.
     *                      Eg: $_SESSION['state'] === bin2hex(random_bytes(16)) . time();
     *                          getAuthorizationUrl($_SESSION['state'], ['basic','email']);
     *
     * @param string $separator Separator to use in http_build_query()
     *
     * @return string
     */
    public function getAuthorizationUrl($state, array $scope = [], $separator = '&')
    {
        $query = http_build_query([
            'client_id' => $this->config['oauth_client_id'],
            'redirect_uri' => $this->config['oauth_callback'],
            'response_type' => 'code',
            'scope' => implode(',', $scope),
            'state' => $state,
        ], null, $separator);

        return Define::getOAuthAuthorizeURL() . '?' . $query;
    }

    /**
     * Exchange code to AccessToken
     *
     * @param $code
     * @return AccessToken
     * @throws \Exception
     */
    public function getAccessTokenFromCode($code)
    {
        if ($this->config['request_type'] === PG::REQUEST_TYPE_CURL) {
            $request = new HttpCurl(Define::getOAuthTokenURL(), 'POST', $this->getExchangeCodeToAccessToken($code),
                [], $this->request_timeout);
            $response = $request->exec();
            $request->close();
            try {
                $response_array = json_decode((string)$response, true);

                if ($response_array['access_token']) {
                    $access_token = new AccessToken($response_array);
                    $pg = PG::getInstance();
                    $pg->setAccessToken($access_token);
                    return $access_token;
                } else {
                    throw new PGOAuthenticationException();
                }
            } catch (PGOAuthorizationException $e) {
                throw $e;
            }
        } else {
            $http = new Client();
            $response = $http->post(Define::getOAuthTokenURL(), [
                'form_params' => $this->getExchangeCodeToAccessToken($code),
            ]);
            try {
                $response_array = json_decode((string)$response, true);
                if ($response_array['access_token']) {
                    $access_token = new AccessToken($response_array);
                    $pg = PG::getInstance();
                    $pg->setAccessToken($access_token);
                    return $access_token;
                } else {
                    throw new PGOAuthenticationException();
                }
            } catch (PGOAuthorizationException $e) {
                throw $e;
            }
        }
    }

    /**
     * Get request data for exchange code to access token
     *
     * @param $code
     * @return array
     */
    private function getExchangeCodeToAccessToken($code)
    {
        return [
            'grant_type' => 'authorization_code',
            'client_id' => $this->config['oauth_client_id'],
            'client_secret' => $this->config['oauth_secret'],
            'redirect_uri' => $this->config['oauth_callback'],
            'code' => $code
        ];
    }
}